# 当发生错误时中止脚本
set -e

# 构建
npm run test:unit
npm run build

# cd 到构建输出的目录
cd dist

git init
git add -A
git commit -m 'deploy'

git push -f https://uu9924079@bitbucket.org/uu9924079/vue-unit-test.git master:gh-pages

cd -

# don't close the terminal when shell is finished
$SHELL