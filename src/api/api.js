import axios from 'axios'

export const addPost = (payload) => {
  return axios.post('https://jsonplaceholder.typicode.com/posts', payload).then(res => res)
}