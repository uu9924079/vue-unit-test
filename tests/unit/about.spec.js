import { shallowMount } from '@vue/test-utils'
import About from '@/views/About.vue'

describe('About.vue', () => {
  it('text will change when button is triggered', () => {
    const wrapper = shallowMount(About)
    const button = wrapper.find('#change-text-btn')
    button.trigger('click')
    expect(wrapper.vm.content).toBe('The content is changed.')
    // expect(wrapper.vm.content).toBe('Expect the test to be failed here!')
  })
})