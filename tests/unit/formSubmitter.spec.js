import { mount } from "@vue/test-utils"
import FormSubmitter from "@/components/FormSubmitter.vue"
import flushPromises from 'flush-promises'
jest.mock('axios', () => ({
    get: jest.fn(() => Promise.resolve({ data: 'data' })),
    post: jest.fn(() => Promise.resolve({ data: 'data' })),
}));

describe("FormSubmitter", () => {
  it("reveals a notification when submitted", async () => {
    const wrapper = mount(FormSubmitter)
    wrapper.find("[data-username]").setValue("alice")
    wrapper.find("form").trigger("submit.prevent") 
    await flushPromises()
    expect(wrapper.find(".message").text()).toBe("Thank you for your submission, alice.")
  })
})