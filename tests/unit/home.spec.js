import { mount } from '@vue/test-utils'
import Home from '@/views/Home'
import HelloWorld from '@/components/HelloWorld'

describe('Home.vue', () => {
  it('bind msg prop to HelloWorld.vue', async () => {
    const wrapper = mount(Home)
    const changedText = 'test change!!'
    await wrapper.setData({ msg: changedText })
    expect(wrapper.findComponent(HelloWorld).vm.$props.msg).toContain(changedText)
  })
})
